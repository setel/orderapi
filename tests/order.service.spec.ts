import {expect} from "chai";
import {Database, OrderStates, IOrderModel, OrderModel} from "../src/database";
import {cancelOrder, checkOrderStatus, createOrder} from "../src/service/order.service";

const clearData = async () => {
    // clean database with productId === "productId"
    await OrderModel.deleteMany({productId: "productId"});
};

describe("Test order service", () => {
    it("should be able to create order", async () => {
        await new Database().init();
        await clearData();

        const productId = "productId";
        const productName = "productName";
        const price = 500;
        const userId = "5c8a0a6955c24f5d2cb8d7db";
        const orderStates = [OrderStates.CONFIRMED, OrderStates.CANCELLED];

        const order: IOrderModel = await createOrder({
            productId,
            productName,
            price
        }, userId);


        expect(order.productId).to.equal(productId);
        expect(order.productName).to.equal(productName);
        expect(orderStates.indexOf(order.states)).to.be.greaterThan(-1);
        expect(order.price).to.equal(price);
    });

    it("should be able to cancel order", async () => {
        const productId = "productId";
        const productName = "productName";
        const orderStates = OrderStates.CANCELLED;

        let order: IOrderModel = await OrderModel.findOne({
            productId
        });

        await cancelOrder(order.id);
        order = await OrderModel.findById(order.id);

        expect(order.productId).to.equal(productId);
        expect(order.productName).to.equal(productName);
        expect(order.states).to.equal(orderStates);
    });

    it("should be able to check order status", async () => {
        const productId = "productId";
        const productName = "productName";
        const orderStates = OrderStates.CANCELLED;

        const order: IOrderModel = await OrderModel.findOne({
            productId
        });
        const orderStatus = await checkOrderStatus(order.id);

        expect(order.productId).to.equal(productId);
        expect(order.productName).to.equal(productName);
        expect(order.states).to.equal(orderStates);
        expect(orderStatus).to.equal(orderStates);

        await clearData();
    });
});
