import {expect} from "chai";
import {Database, UserRole, UserStatus, IUserModel, UserModel} from "../src/database";
import {createUser} from "../src/service/user.service";

const clearData = async () => {
    // clean database with email === "test@yahoo.com"
    await UserModel.deleteMany({email: "test@yahoo.com"});
};

describe("Test user service", () => {
    it("should be able to create user", async () => {
        await new Database().init();
        await clearData();

        const email = "test@yahoo.com";
        const role = UserRole.ADMIN;

        const user: IUserModel = await createUser({
            email,
            role
        });

        expect(user.email).to.equal(email);
        expect(user.status).to.equal(UserStatus.ACTIVE);
        expect(user.role).to.equal(role);

        await clearData();
    });
});
