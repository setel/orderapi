import {IOrderModel, OrderModel, OrderStates} from "../database";
import {PaymentAPI} from "../integration/payment";
import {ICreateOrderRequest} from "../typings/order";
import {getUser} from "./controller.service";

export const orderList = async (): Promise<IOrderModel[]> => {
    return await OrderModel.find({});
};

export const createOrder = async (request: ICreateOrderRequest, userId: string | undefined): Promise<IOrderModel> => {
    const user = await getUser(userId);
    if (user) {
        let newOrder = new OrderModel({
            price: request.price,
            productId: request.productId,
            productName: request.productName,
            states: OrderStates.CREATED,
            userId: user.id
        });

        newOrder = await newOrder.save();
        const success = await PaymentAPI.makePayment(user.id, {
            orderId: newOrder.id
        });

        if (!success) {
            newOrder.states = OrderStates.CANCELLED;
        } else {
            newOrder.states = OrderStates.CONFIRMED;
        }

        newOrder = await newOrder.save();
        if (success) {
            setTimeout(async () => {
                const order = await OrderModel.findById(newOrder.id);
                if (order.states === OrderStates.CONFIRMED) {
                    order.states = OrderStates.DELIVERED;
                    await order.save();
                }
            }, 10000);
        }

        return newOrder;
    } else {
        throw new Error("User not found");
    }
};

export const cancelOrder = async (orderId: string): Promise<void> => {
    const order: IOrderModel = await OrderModel.findById(orderId);
    if (order) {
        order.states = OrderStates.CANCELLED;
        await order.save();
    }
};

export const checkOrderStatus = async (orderId: string): Promise<OrderStates | undefined> => {
    const order: IOrderModel = await OrderModel.findById(orderId);
    if (order) {
        return order.states;
    }
};
