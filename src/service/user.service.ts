import {IUserModel, UserModel, UserStatus} from "../database";
import {ILoginRequest} from "../typings/order";
import {ICreateUserRequest} from "../typings/user";

export const createUser = async (request: ICreateUserRequest): Promise<IUserModel> => {
    const userModel: IUserModel = new UserModel({
        email: request.email,
        role: request.role,
        status: UserStatus.ACTIVE
    });
    return await userModel.save();
};

export const login = async (request: ILoginRequest): Promise<string> => {
    const userModel: IUserModel = await UserModel.findOne({
        email: request.email
    });

    if (userModel) {
        return userModel.id;
    } else {
        throw Error("User not found");
    }
};
