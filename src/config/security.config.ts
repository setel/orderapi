export const permissions: Array<{url: string, method: string, role: string[]}> = [
    {
        method: "get",
        role: ["*"],
        url: ""
    }, {
        method: "post",
        role: ["*"],
        url: "/login"
    }, {
        method: "get",
        role: ["admin", "customer"],
        url: "/orders"
    }, {
        method: "post",
        role: ["admin", "customer"],
        url: "/orders/create"
    }, {

        method: "post",
        role: ["admin", "customer"],
        url: "/orders/cancel/**"
    }, {
        method: "get",
        role: ["admin", "customer"],
        url: "/orders/status/**"
    }
];
