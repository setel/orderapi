import axios, {AxiosResponse} from "axios";
import {IPaymentRequest} from "../typings/payment";
import {paymentConfig} from "./payment.config";

export class PaymentAPI {
    public static async makePayment(userId: string, request: IPaymentRequest): Promise<boolean> {
        try {
            const response: AxiosResponse = await axios.post(`${paymentConfig.host}/payment`, request,
                {headers: {Authorization: userId}});
            if (response.status === 200) {
                return response.data;
            } else {
                return false;
            }
        } catch (error) {
            console.error("makePayment error: ", error.response);
            return false;
        }
    }
}
