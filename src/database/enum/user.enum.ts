export enum UserStatus {
    ACTIVE = "active",
    INACTIVE = "inactive",
}

export enum UserRole {
    CUSTOMER = "customer",
    ADMIN = "admin",
}
