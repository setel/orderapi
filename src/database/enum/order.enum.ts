export enum OrderStates {
    CREATED = "created",
    CONFIRMED = "confirmed",
    DELIVERED = "delivered",
    CANCELLED = "cancelled"
}
