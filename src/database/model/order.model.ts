import {Document, Model, model, Schema} from "mongoose";
import {OrderStates} from "../enum/order.enum";

export interface IOrderModel extends Document {
    createdDate?: Date;
    price: number;
    productId: string;
    productName: string;
    states: OrderStates;
    userId: string;
}

const OrderSchema = new Schema({
    createdDate: Date,
    price: Number,
    productId: {
        index: true,
        required: true,
        type: String
    },
    productName: String,
    states: {
        enum: Object.values(OrderStates),
        index: true,
        required: true,
        type: String
    },
    userId: {
        ref: "user",
        type: Schema.Types.ObjectId
    }
});

OrderSchema.pre("save", (next) => {
    if (!this.date) {
        this.date = new Date();
    }
    next();
});

export const OrderModel: Model<IOrderModel> = model<IOrderModel>("order", OrderSchema);
