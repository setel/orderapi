import mongoose = require("mongoose");
import {databaseConfig} from "./config/database.config";

export class Database {
    public async init() {
        await mongoose.connect(databaseConfig.host, { autoIndex: false, useNewUrlParser: true });
    }
}

export * from "./enum/order.enum";
export * from "./enum/user.enum";

export * from "./model/order.model";
export * from "./model/user.model";
