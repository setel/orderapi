export interface IResponseError {
    code: number,
    status: string,
    message: string,
    errors: string[]
}

export interface IValidate {
    valid: boolean | string | number,
    resp: string | Function
}
