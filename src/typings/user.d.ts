import {UserRole} from "../database";

export interface ICreateUserRequest {
    email: string,
    role: UserRole,
}
