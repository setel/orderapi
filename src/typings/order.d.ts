export interface ILoginRequest {
    email: string
}

export interface ICreateOrderRequest {
    productId: string,
    productName: string,
    price: number
}
