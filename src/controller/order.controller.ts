import {Express, Request, Response} from "express";
import {controllerProcess} from "../service/controller.service";
import {cancelOrder, checkOrderStatus, createOrder, orderList} from "../service/order.service";
import {login} from "../service/user.service";
import {IValidate} from "../typings/controller";
import {ICreateOrderRequest, ILoginRequest} from "../typings/order";

export class OrderController {
    public init(express: Express) {
        express.get("/", async (request: Request, response: Response) => {
            const validate: IValidate = {
                resp: "",
                valid: true
            };

            const exec = async () => "OrderAPI is up";
            await controllerProcess(validate, exec, response);
        });

        express.post("/login", async (request: Request, response: Response) => {
            const loginRequest = request.body as ILoginRequest;
            const validate: IValidate = {
                resp: "email is required",
                valid: loginRequest.email
            };

            const exec = async () => login(loginRequest);
            await controllerProcess(validate, exec, response);
        });

        express.get("/orders", async (request: Request, response: Response) => {
            const validate: IValidate = {
                resp: "",
                valid: true
            };

            const exec = async () => orderList();
            await controllerProcess(validate, exec, response);
        });

        express.get("/orders/status/:oid", async (request: Request, response: Response) => {
            const validate: IValidate = {
                resp: "Order id is required",
                valid: request.params.oid
            };

            const exec = async () => checkOrderStatus(request.params.oid);
            await controllerProcess(validate, exec, response);
        });

        express.post("/orders/create", async (request: Request, response: Response) => {
            const createOrderRequest = request.body as ICreateOrderRequest;
            const validate: IValidate[] = [{
                resp: "product id is required",
                valid: createOrderRequest.productId
            }, {
                resp: "product name is required",
                valid: createOrderRequest.productName
            }];

            const exec = async () => createOrder(createOrderRequest, request.headers.authorization);
            await controllerProcess(validate, exec, response);
        });

        express.post("/orders/cancel/:oid", async (request: Request, response: Response) => {
            const validate: IValidate = {
                resp: "Order id is required",
                valid: request.params.oid
            };

            const exec = async () => cancelOrder(request.params.oid);
            await controllerProcess(validate, exec, response);
        });
    }
}
